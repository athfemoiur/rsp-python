from config import CHOICES, GAME_STATES
import random
import Decerators

results = {'user': 0, 'system': 0}
scoreboard = {'user': 0, 'system': 0}


def get_user_choice():
    user_input = input('pick up a choice (r, s, p):  ')

    if user_input not in CHOICES:
        print('Error! Invalid choice')
        return get_user_choice()

    return user_input


def get_system_choice():
    return random.choice(CHOICES)


def get_winner(user, system):
    choices = (user, system)

    if choices not in GAME_STATES:
        return 'Draw'

    return GAME_STATES[choices]


def update_state():
    if results['user'] == 3:
        scoreboard['user'] += 1
        msg = 'Nice! You won this round!!'
    else:
        scoreboard['system'] += 1
        msg = 'You lost this round!!'

    print('______________________________________________________')
    print(f"You : {results['user']} , System : {results['system']}")
    print('------------------------------------------------------')
    print(msg)
    print('------------------------------------------------------')
    print(f"total result : {scoreboard}")
    print('------------------------------------------------------')

    results['user'], results['system'] = 0, 0


@Decerators.time_play_decorator
def play_one_round():
    print('Welcome to r, s, p !')

    while results['user'] < 3 and results['system'] < 3:
        user_input = get_user_choice()
        system_input = get_system_choice()

        if get_winner(user_input, system_input) == 'Win':
            print('Nice!')
            print(f"your choice : {user_input}, system choice : {system_input}")
            results['user'] += 1

        elif get_winner(user_input, system_input) == 'Loss':
            print('Bad')
            print(f"your choice : {user_input}, system choice : {system_input}")

            results['system'] += 1

        else:
            print('Not bad')
            print(f"your choice : {user_input}, system choice : {system_input}")

    update_state()


@Decerators.time_play_decorator
def play():
    while True:
        play_one_round()
        print("----------------------------------------")
        answer = input('Do you want to continue? ')
        if answer == 'y':
            continue
        else:
            print("----------------------------------------")
            print('End!  ')
            break


if __name__ == '__main__':
    play()
