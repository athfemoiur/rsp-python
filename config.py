CHOICES = ('r', 's', 'p')

GAME_STATES = {
    ('r', 's'): 'Win',
    ('s', 'p'): 'Win',
    ('p', 'r'): 'Win',
    ('s', 'r'): 'Loss',
    ('p', 's'): 'Loss',
    ('r', 'p'): 'Loss'
}


