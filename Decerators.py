from datetime import datetime


def time_play_decorator(func):

    def wrapper_time():
        start_time = datetime.now()
        func()
        stop_time = datetime.now()
        print("________________________________________________")
        print(f" time : {(stop_time - start_time).seconds}")

    return wrapper_time
